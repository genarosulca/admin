package ec.com.coge.tus.seis.admin;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ec.com.coge.tus.seis.core.dto.UsuarioDTO;
import ec.com.coge.tus.seis.core.servicio.IUsuarioServicio;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AdminApplicationTests {

	@Autowired
	private IUsuarioServicio usuarioServicio;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void obtenerTodosUsuarios() {
		Collection<UsuarioDTO> userDetails = usuarioServicio.obtenerUsuarios();
		for (UsuarioDTO usuarioDTO : userDetails) {
			System.out.println("*******************************************");
			System.out.println(usuarioDTO.getPrimerNombre());
			System.out.println(usuarioDTO.getPrimerApellido());
			System.out.println("*******************************************");
		}
	}

}
