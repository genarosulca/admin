package ec.com.coge.tus.seis.admin.util;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Component;

import ec.com.coge.tus.seis.admin.to.UsuarioTO;
import ec.com.coge.tus.seis.core.dto.UsuarioDTO;

@Component
public class LoginUtil {

	public UsuarioTO convertirUsuarioDTO(UsuarioDTO usuarioDto) {
		UsuarioTO usuario = new UsuarioTO();
		usuario.setPrimerNombre(usuarioDto.getPrimerNombre());
		usuario.setPrimerApellido(usuarioDto.getPrimerApellido());
		return usuario;
	}
	
	public Collection<UsuarioTO> convertirUsuariosDTO(Collection<UsuarioDTO> usuariosDto){
		Collection<UsuarioTO> usuarios = new ArrayList<>();
		for (UsuarioDTO usuarioDTO : usuariosDto) {
			usuarios.add(convertirUsuarioDTO(usuarioDTO));
		}
		return usuarios;
	}
}
