package ec.com.coge.tus.seis.admin.webservices;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ec.com.coge.tus.seis.admin.to.UsuarioTO;
import ec.com.coge.tus.seis.admin.util.LoginUtil;
import ec.com.coge.tus.seis.core.dto.UsuarioDTO;
import ec.com.coge.tus.seis.core.servicio.IUsuarioServicio;

@Controller // This means that this class is a Controller
@RequestMapping(path = "/login") // This means URL's start with /demo (after Application path)
public class LoginWebservice {

	@Autowired
	private IUsuarioServicio usuarioServicio;
	@Autowired
	private LoginUtil loginUtil;
	
	@RequestMapping(path = "/user", method = RequestMethod.GET)
	// @GetMapping(path="/user")
	public @ResponseBody UsuarioTO getUser(@RequestParam Integer userId) {

		UsuarioTO usuario = new UsuarioTO();
		usuario.setIdUsuario(userId);
		usuario.setPrimerNombre("Genaro");
		return usuario;
	}
	
	@RequestMapping(path = "/todos", method = RequestMethod.GET)
	public ResponseEntity<Collection<UsuarioTO>> todos() {
		

		Collection<UsuarioDTO> userDetails = usuarioServicio.obtenerUsuarios();
		Collection<UsuarioTO> response = loginUtil.convertirUsuariosDTO(userDetails);
		return new ResponseEntity<Collection<UsuarioTO>>(response, HttpStatus.OK);
	}
}
